# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GORUN=$(GOCMD) run
BINARY_NAME=tplus


all: build
build:
		GOOS=linux GOARCH=amd64 $(GOBUILD) -o ./dist/$(BINARY_NAME)_linux_amd64 *.go
		GOOS=windows GOARCH=amd64 $(GOBUILD) -o ./dist/$(BINARY_NAME)_win_amd64.exe *.go