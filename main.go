package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	cmd2 "gitlab.com/tuliptools/TplusCLI/internal/cmd"
	"gitlab.com/tuliptools/TplusCLI/internal/config"
	db2 "gitlab.com/tuliptools/TplusCLI/internal/db"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
	"os"
)

func main() {

	must := func(e error) {
		if e != nil {
			panic(e)
		}
	}

	c := dig.New()
	must(c.Provide(func() *logrus.Logger {
		val, ok := os.LookupEnv("TPLUS_CLI_DEBUG")
		l := logrus.New()
		l.SetFormatter(&logrus.TextFormatter{})
		if ok && val == "1" {
			l.SetLevel(logrus.TraceLevel)
		}
		if ok && val == "2" {
			l.SetLevel(logrus.DebugLevel)
		}
		if !ok || val == "0" {
			l.SetLevel(logrus.ErrorLevel)
		}

		return l
	}))

	must(c.Provide(config.NewConfig))
	must(c.Provide(db2.NewDB))
	must(c.Provide(service.NewServerService))
	must(c.Provide(service.NewServerClient))
	must(c.Provide(service.NewEnvService))
	must(c.Provide(service.NewProjectService))
	must(c.Provide(service.NewTaskService))
	must(c.Provide(service.NewVmService))

	if err := cmd2.GetRootCmd(c).Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

}
