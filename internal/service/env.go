package service

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/nsf/termbox-go"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/TplusCLI/internal/db"
	"gitlab.com/tuliptools/TplusCLI/internal/models"
	"golang.org/x/net/websocket"
	"io"
	"log"
	"os"
	"strings"
)

type EnvService struct {
	db     *db.DB
	client *ServerClient
	log    *logrus.Entry
}

func NewEnvService(db *db.DB, client *ServerClient, log *logrus.Logger) *EnvService {
	s := EnvService{}
	s.db = db
	s.client = client
	s.log = log.WithField("source", "EnvService")
	return &s
}

func (envs *EnvService) GetConsole(console string) (io.Reader, io.Writer, error) {
	qc := envs.db.GetQuickConf()
	token, err := envs.client.GetWSToken(qc.ActiveEndpoint, qc.ActiveToken, qc.ActiveEnvID, console)
	if err != nil {
		return nil, nil, errors.New("Error obtaining token for websocket connection...")
	}

	wsurl := qc.ActiveEndpoint + "/env/" + qc.ActiveEnvID + "/ws/" + token
	wsurl = strings.Replace(wsurl, "https", "wss", 1)
	wsurl = strings.Replace(wsurl, "http", "ws", 1)
	wsurl = strings.Replace(wsurl, "//env", "/env", -1)

	ws, err := websocket.Dial(wsurl, "", "http://localhost")
	if err != nil {
		log.Fatal(err)
	}

	termbox.Init()
	if err != nil {
		panic(err)
	}
	termbox.SetInputMode(termbox.InputEsc)

	go func() {
		in := bufio.NewScanner(os.Stdin)
		in.Split(bufio.ScanBytes)
		for in.Scan() {
			ws.Write(in.Bytes())
		}
		termbox.Interrupt()
	}()

	go func() {
		writer2 := bufio.NewScanner(ws)
		writer2.Split(bufio.ScanRunes)
		for writer2.Scan() {
			s := writer2.Bytes()
			s = bytes.Replace(s, []byte("\n"), []byte("\n\r"), -1)
			var sn []byte
			for _, k := range s {
				if true {
					sn = append(sn, k)
				}
			}
			fmt.Print(string(sn))
		}
		termbox.Interrupt()
	}()

mainloop:
	for {
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:
			if ev.Key == termbox.KeyEsc {
				termbox.Interrupt()
			}
			ws.Write([]byte{byte(ev.Ch)})

		case termbox.EventError:
			termbox.Interrupt()

		case termbox.EventInterrupt:
			break mainloop
		}

	}
	termbox.Close()

	return nil, nil, nil
}

func (envs *EnvService) GetLogs(e *models.Environment, tail int) ([]string, error) {
	qc := envs.db.GetQuickConf()
	return envs.client.GetLogs(qc.ActiveEndpoint, qc.ActiveToken, e.Id, tail)
}

func (envs *EnvService) GetActiveEnv() *models.Environment {
	qc := envs.db.GetQuickConf()
	if qc.ActiveEnvID == "" {
		fmt.Println("Set an active env first!")
		os.Exit(1)
	}
	e, _ := envs.GetEnvironmentById(qc.ActiveEnvID)
	return &e
}

func (envs *EnvService) GetEnvironmentById(id string) (res models.Environment, err error) {
	qc := envs.db.GetQuickConf()
	if qc.ActiveEndpoint == "" {
		return res, errors.New("No active server!")
	}
	res, err = envs.client.GetEnvById(qc.ActiveEndpoint, qc.ActiveToken, id)
	return res, nil
}

func (envs *EnvService) GetEnvironments() (res []models.Environment, err error) {
	qc := envs.db.GetQuickConf()
	if qc.ActiveEndpoint == "" {
		return res, errors.New("No active server!")
	}
	res, err = envs.client.GetEnvs(qc.ActiveEndpoint, qc.ActiveToken)
	return res, nil
}

func (envs *EnvService) StartEnvironment(e *models.Environment) (err error) {
	qc := envs.db.GetQuickConf()
	if qc.ActiveEndpoint == "" {
		return errors.New("No active server!")
	}
	err = envs.client.StartEnv(qc.ActiveEndpoint, qc.ActiveToken, e.Id)
	return nil
}

func (envs *EnvService) RestartEnvironment(e *models.Environment) (err error) {
	qc := envs.db.GetQuickConf()
	if qc.ActiveEndpoint == "" {
		return errors.New("No active server!")
	}
	err = envs.client.RestartEnv(qc.ActiveEndpoint, qc.ActiveToken, e.Id)
	return nil
}

func (envs *EnvService) CleanEnvironment(e *models.Environment) (err error) {
	qc := envs.db.GetQuickConf()
	if qc.ActiveEndpoint == "" {
		return errors.New("No active server!")
	}
	err = envs.client.CleanEnv(qc.ActiveEndpoint, qc.ActiveToken, e.Id)
	return nil
}

func (envs *EnvService) DestroyEnvironment(e *models.Environment) (err error) {
	qc := envs.db.GetQuickConf()
	if qc.ActiveEndpoint == "" {
		return errors.New("No active server!")
	}
	err = envs.client.DestroyEnv(qc.ActiveEndpoint, qc.ActiveToken, e.Id)
	return nil
}

func (envs *EnvService) StopEnvironment(e *models.Environment) (err error) {
	qc := envs.db.GetQuickConf()
	if qc.ActiveEndpoint == "" {
		return errors.New("No active server!")
	}
	err = envs.client.StopEnv(qc.ActiveEndpoint, qc.ActiveToken, e.Id)
	return nil
}

func (envs *EnvService) CreateEnvironment(add models.AddEnvInput) error {
	qc := envs.db.GetQuickConf()
	if qc.ActiveEndpoint == "" {
		return errors.New("No active server!")
	}
	e := envs.client.CreateEnv(qc.ActiveEndpoint, qc.ActiveToken, add)
	if e != nil {
		envs.log.Debug("Error creating env: ", e)
		return errors.New("Could not create Env")
	}
	return nil
}
