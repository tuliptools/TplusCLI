package service

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.com/tuliptools/TplusCLI/internal/models"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

type ServerClient struct {
	http.Client
}

func NewServerClient() *ServerClient {
	tr := &http.Transport{
		MaxIdleConns:       50,
		IdleConnTimeout:    5 * time.Second,
		DisableCompression: false,
	}
	s := ServerClient{Client: http.Client{Transport: tr}}
	return &s
}

func (sc *ServerClient) GetToken(host, user, pass string) (string, error) {
	url := host + "/token"
	var jsonStr = []byte(`{"username":"` + user + `","password":"` + pass + `"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	resp, err := sc.Do(req)
	if err != nil {
		return "", errors.New("Error obtaining token, check your login info!")
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	type Response struct {
		Token string `json:"Token"`
	}

	response := Response{}
	if err != nil {
		return "", err
	}
	json.Unmarshal(body, &response)

	return response.Token, nil
}

func (sc *ServerClient) CreateEnv(host, token string, add models.AddEnvInput) error {
	url := host + "/env/create"
	jsonBytes, _ := json.Marshal(add)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonBytes))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	defer resp.Body.Close()
	if err != nil {
		return err
	}
	return nil
}

func (sc *ServerClient) GetLogs(host, token, envid string, n int) ([]string, error) {
	var res []string
	url := host + "/env/" + envid + "/logs/tezos/" + strconv.Itoa(n)
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}
	if resp.StatusCode != 200 {
		return res, errors.New("cant connect to the server")
	}
	json.Unmarshal(body, &res)
	return res, nil
}

func (sc *ServerClient) GetFile(host, token, envid string, file string) ([]byte, error) {
	var res []byte
	url := host + "/bash/" + envid + "/down/" + base64.StdEncoding.EncodeToString([]byte(file))
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}
	if resp.StatusCode != 200 {
		return res, errors.New("cant connect to the server")
	}
	return body, nil
}

func (sc *ServerClient) GetWSToken(host, token, envid, console string) (string, error) {
	res := ""
	url := host + "/env/" + envid + "/console/" + console
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}
	if resp.StatusCode != 200 {
		return res, errors.New("cant connect to the server")
	}
	return string(body), nil
}

func (sc *ServerClient) GetEnvById(host, token, id string) (models.Environment, error) {
	res := models.Environment{}
	url := host + "/env/" + id
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}
	if resp.StatusCode != 200 {
		return res, errors.New("cant connect to the server")
	}
	json.Unmarshal(body, &res)
	return res, nil
}

func (sc *ServerClient) GetEnvs(host, token string) ([]models.Environment, error) {
	var res []models.Environment
	url := host + "/env"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}
	if resp.StatusCode != 200 {
		return res, errors.New("cant connect to the server")
	}
	json.Unmarshal(body, &res)
	return res, nil
}

func (sc *ServerClient) StopEnv(host, token string, envid string) error {
	url := host + "/env/" + envid + "/stop"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return errors.New("cant connect to the server")
	}
	return nil

}

func (sc *ServerClient) StartEnv(host, token string, envid string) error {
	url := host + "/env/" + envid + "/start"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return err
	}
	return nil

}

func (sc *ServerClient) CleanEnv(host, token string, envid string) error {
	url := host + "/env/" + envid + "/clean"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return errors.New("cant connect to the server")
	}
	return nil
}

func (sc *ServerClient) RestartEnv(host, token string, envid string) error {
	url := host + "/env/" + envid + "/restart"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return errors.New("cant connect to the server")
	}
	return nil

}

func (sc *ServerClient) DestroyEnv(host, token string, envid string) error {
	url := host + "/env/" + envid + "/destroy"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return errors.New("cant connect to the server")
	}
	return nil

}

func (sc *ServerClient) GetProject(host, token string, envid string, projectId string) (models.Project, error) {
	res := models.Project{}
	url := host + "/env/" + envid + "/projects/" + projectId
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}
	if resp.StatusCode != 200 {
		return res, errors.New("cant connect to the server")
	}
	json.Unmarshal(body, &res)
	return res, nil
}

func (sc *ServerClient) GetProjects(host, token string, envid string,) ([]models.Project, error) {
	res := []models.Project{}
	url := host + "/env/" + envid + "/projects"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}
	if resp.StatusCode != 200 {
		return res, errors.New("cant connect to the server")
	}
	json.Unmarshal(body, &res)
	return res, nil
}


func (sc *ServerClient) RunTask(host, token string, envid string, projectId, task string) (models.ProjectTask, error) {
	res := models.ProjectTask{}
	url := host + "/project/" + envid + "/" + projectId + "/run/" + task
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}
	if resp.StatusCode != 200 {
		return res, errors.New("cant connect to the server")
	}
	json.Unmarshal(body, &res)
	return res, nil
}

func (sc *ServerClient) GetTask(host, token string, envid string, projectId, task string) (models.ProjectTask, error) {
	res := models.ProjectTask{}
	url := host + "/project/" + envid + "/" + projectId + "/get/" + task
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)

	resp, err := sc.Do(req)
	if err == nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return res, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return res, err
	}
	if resp.StatusCode != 200 {
		return res, errors.New("cant connect to the server")
	}
	json.Unmarshal(body, &res)
	return res, nil
}

func (sc *ServerClient) GetProjectDown(host, token string, envid string, projectId string) (io.Reader, error) {
	res := bytes.NewReader([]byte{})
	url := host + "/project/" + envid + "/" + projectId + "/down"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)
	if err != nil {
		return res, err
	}
	resp, err := sc.Do(req)
	if err != nil {
		return res, err
	}
	return resp.Body, nil
}

func (sc *ServerClient) ProjectUp(host, token string, envid string, projectId string, buffer *bytes.Buffer) error {
	url := host + "/project/" + envid + "/" + projectId + "/up"
	req, err := http.NewRequest("POST", url, buffer)
	req.Header.Set("TOKEN", token)
	if err != nil {
		return err
	}
	_, err = sc.Do(req)
	if err != nil {
		return err
	}
	return nil
}


func (sc *ServerClient) ProjectCreate(host, token, envid string, params models.CreateProjectParams) string {
	url := host + "/projects/" + envid + "/new"
	paramsBytes,_ := json.Marshal(params)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(paramsBytes))
	req.Header.Set("TOKEN", token)
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return ""
	}
	res, err := sc.Do(req)
	if err != nil {
		return ""
	}
	body,_ := ioutil.ReadAll(res.Body)
	return string(body)
}

func (sc *ServerClient) ProjectVars(host, token, envid,pid string) *models.ProjectVars {
	url := host + "/project/" + envid + "/" + pid + "/variables"
	req, err := http.NewRequest("GET", url, bytes.NewBuffer([]byte{}))
	req.Header.Set("TOKEN", token)
	if err != nil {
		return nil
	}
	res, err := sc.Do(req)
	if err != nil {
		return nil
	}
	body,_ := ioutil.ReadAll(res.Body)
	model := models.ProjectVars{}
	json.Unmarshal(body, &model)
	return &model
}