package service

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/TplusCLI/internal/db"
	"time"
)

type ServerService struct {
	db     *db.DB
	client *ServerClient
	log    *logrus.Entry
}

func NewServerService(db *db.DB, client *ServerClient, log *logrus.Logger) *ServerService {
	s := ServerService{}
	s.db = db
	s.client = client
	s.log = log.WithField("source", "ServerService")
	return &s
}

func (serser *ServerService) AddServer(host, user, pass, name string) error {
	token, err := serser.client.GetToken(host, user, pass)
	if err != nil {
		return err
	}
	if len(token) <= 10 {
		return errors.New("Could not get Token")
	}
	server := &db.Server{
		ID:        name,
		Endpoint:  host,
		Name:      name,
		CreatedAt: time.Now(),
		LastUsed:  time.Now(),
		Token:     token,
		Username:  user,
	}
	return serser.db.AddServer(server)
}

func (serser *ServerService) GetServerByName(name string) *db.Server {
	res, e := serser.db.GetServerByName(name)
	if e != nil {
		serser.log.Debug("Error getting server: ", e)
	}
	return &res
}

func (serser *ServerService) RemoveServer(s *db.Server) error {
	e := serser.db.RemoveServer(s)
	if e != nil {
		serser.log.Debug("error removing server: ", e)
		return errors.New("Could not remove Server")
	}
	return nil
}
