package service

import (
	"fmt"
	"github.com/bmatcuk/go-vagrant"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/TplusCLI/internal/db"
	"io/ioutil"
	"os"
	"time"
)

type VmService struct {
	db        *db.DB
	client    *ServerClient
	log       *logrus.Entry
	syncstate string
	vmdir     string
	servers   *ServerService
}

func NewVmService(db *db.DB, client *ServerClient, log *logrus.Logger, servers *ServerService) *VmService {
	s := VmService{}
	s.db = db
	s.client = client
	s.servers = servers
	s.log = log.WithField("source", "VM")
	s.Init()
	return &s
}

func (this *VmService) Init() {
	dir, _ := os.UserHomeDir()
	this.vmdir = dir + "/.tplusbox/"
	fmt.Println(this.vmdir)
	os.MkdirAll(this.vmdir, 0777)
}
func (this *VmService) writeVagrantFile() {
	file :=
		`Vagrant.configure("2") do |config|
  config.vm.box = "tuliptools/tplus"
  config.vm.box_version = "1.0.2"

  config.vm.network "private_network", ip: "172.16.0.100"

  config.vm.provider "virtualbox" do |v|
    v.name = "tplus_box"
    v.memory = 4096
    v.cpus = 2
  end

end`
	err := ioutil.WriteFile(this.vmdir+"Vagrantfile", []byte(file), 0644)
	if err != nil {
		this.log.Panic("Could not write Vagrantfile")
	}

}

func (this *VmService) Up() {
	this.writeVagrantFile()
	client, err := vagrant.NewVagrantClient(this.vmdir)
	if err != nil {
		this.log.Panic("Cant create vagrant instance")
	}
	upcmd := client.Up()
	upcmd.Verbose = true

	fmt.Println("Starting Tplus box")
	if err := upcmd.Run(); err != nil {
		this.log.Panic(err)
	}
	if upcmd.Error != nil {
		this.log.Panic(upcmd.Error)
	}

	fmt.Println("Tplus VM up and running!")
	fmt.Println("VM has IP: 172.16.0.100")
	fmt.Println("Access the UI under: http://172.16.0.100/ui")
	fmt.Println("Default Credentials:")
	fmt.Println("   developer / developer")
	fmt.Println("")
	time.Sleep(3 * time.Second)
	a := this.servers.GetServerByName("vm")
	if a != nil && a.ID != "" {
		this.servers.RemoveServer(a)
	}
	this.servers.AddServer("http://172.16.0.100", "developer", "developer", "vm")
	this.db.SetActiveServer(this.servers.GetServerByName("vm"))
	fmt.Println("This CLI tool has been configured to use the vm per default!")

}

func (this *VmService) Down() {
	this.writeVagrantFile()
	client, err := vagrant.NewVagrantClient(this.vmdir)
	if err != nil {
		this.log.Panic("Cant create vagrant instance")
	}
	downcmd := client.Suspend()
	downcmd.Verbose = true

	fmt.Println("Stopping Tplus box")
	if err := downcmd.Run(); err != nil {
		this.log.Panic(err)
	}
	if downcmd.Error != nil {
		this.log.Panic(downcmd.Error)
	}

	fmt.Println("Tplus VM stopped!")
}

func (this *VmService) Destroy() {
	this.writeVagrantFile()
	client, err := vagrant.NewVagrantClient(this.vmdir)
	if err != nil {
		this.log.Panic("Cant create vagrant instance")
	}
	rmcmd := client.Destroy()
	rmcmd.Verbose = true

	fmt.Println("Removing Tplus box")
	if err := rmcmd.Run(); err != nil {
		this.log.Panic(err)
	}
	if rmcmd.Error != nil {
		this.log.Panic(rmcmd.Error)
	}

	fmt.Println("All traces of the Tplus VM removed.")
}
