package service

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/TplusCLI/internal/db"
	"os"
	"time"
)

type TaskService struct {
	db      *db.DB
	client  *ServerClient
	log     *logrus.Entry
	project *ProjectService
}

func NewTaskService(db *db.DB, client *ServerClient, log *logrus.Logger, p *ProjectService) *TaskService {
	s := TaskService{}
	s.db = db
	s.client = client
	s.project = p
	s.log = log.WithField("source", "TaskService")
	return &s
}

func (ts *TaskService) Run(filename string) {
	ts.project.Up()
	id := ts.runTask(filename)
	if id != "" {
		pj, _ := ts.project.ProjectJson()
		qc := ts.db.GetQuickConf()
		i := 0
		for {
			task, e := ts.client.GetTask(qc.ActiveEndpoint, qc.ActiveToken, qc.ActiveEnvID, pj.ProjectIds[qc.ActiveEnvID], id)
			if e != nil {
				fmt.Println(e)
				os.Exit(1)
			}
			if task.ID == id {
				if len(task.Logs) >= i {
					for j, l := range task.Logs {
						if j >= i {
							i = j + 1
							fmt.Println(l)
						}
					}
				}
			}
			if task.Finished {
				break
			}
			time.Sleep(1 * time.Second)
		}
	}
	ts.project.Down()
}

func (ts *TaskService) runTask(filename string) string {
	qc := ts.db.GetQuickConf()
	pj, _ := ts.project.ProjectJson()
	t, _ := ts.client.RunTask(qc.ActiveEndpoint, qc.ActiveToken, qc.ActiveEnvID, pj.ProjectIds[qc.ActiveEnvID], filename)
	return t.ID
}
