package service

import (
	"archive/tar"
	"bytes"
	. "compress/gzip"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/TplusCLI/internal/db"
	"gitlab.com/tuliptools/TplusCLI/internal/models"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type ProjectService struct {
	db        *db.DB
	client    *ServerClient
	log       *logrus.Entry
	syncstate string
}

func NewProjectService(db *db.DB, client *ServerClient, log *logrus.Logger) *ProjectService {
	s := ProjectService{}
	s.db = db
	s.client = client
	s.log = log.WithField("source", "ProjectService")
	return &s
}

func (ps *ProjectService) checkGitIgnore(){
	// make sure we are in project root
	_,err1 := ps.ProjectJson()
	if err1 == nil {
		if info, err2 := os.Stat("./.gitignore"); err2 == nil {
			// file exists
			content,err3 := ioutil.ReadFile("./.gitignore")
			if err3 == nil && !bytes.Contains(content, []byte(".tplus-project.json")) {
				content = append(content, []byte("\n.tplus-project.json")...)
				ioutil.WriteFile(".gitignore",content,info.Mode())
			}
		} else {
			content := []byte(".tplus-project.json\n")
			ioutil.WriteFile(".gitignore",content,0666)
		}
	}

}

func (ps *ProjectService) Init(id string) (models.Project, error) {
	qc := ps.db.GetQuickConf()
	return ps.client.GetProject(qc.ActiveEndpoint, qc.ActiveToken, qc.ActiveEnvID, id)
}

func (ps *ProjectService) ProjectJson() (*models.ProjectJson, error) {
	res := models.ProjectJson{
		ProjectIds: map[string]string{},
	}
	f, e := ioutil.ReadFile("./.tplus-project.json")
	if e != nil {
		return &res, e
	}
	e = json.Unmarshal(f, &res)

	// valdiate if all projects really exist

	qc := ps.db.GetQuickConf()
	projects,e := ps.client.GetProjects(qc.ActiveEndpoint,qc.ActiveToken,qc.ActiveEnvID)
	newmap := map[string]string{}
	for a,b := range res.ProjectIds {
		if a != "" {
			if a == qc.ActiveEnvID {
				for _,c := range projects {
					if c.ID == b {
						newmap[a] = c.ID
					}
				}
			} else {
				newmap[a] = b
			}
		}
	}
	res.ProjectIds = newmap
	ps.SaveProjectJson(&res)
	return &res, nil
}

func (ps *ProjectService) TplusJson() (*models.TplusJson, error) {
	res := models.TplusJson{}
	f, e := ioutil.ReadFile("./.tplus.json")
	if e != nil {
		return nil, e
	}
	e = json.Unmarshal(f, &res)
	return &res, nil
}

func (ps *ProjectService) SaveProjectJson(p *models.ProjectJson) error {
	bytes, e := json.MarshalIndent(p, "", "  ")
	if e != nil {
		return e
	}
	return ioutil.WriteFile("./.tplus-project.json", bytes, 0666)
}

func (ps *ProjectService) SaveTplusJson(p *models.TplusJson) error {
	bytes, e := json.MarshalIndent(p, "", "  ")
	if e != nil {
		return e
	}
	return ioutil.WriteFile("./.tplus.json", bytes, 0666)
}

func (ps *ProjectService) Down() error {
	qc := ps.db.GetQuickConf()
	pj, e := ps.ProjectJson()
	if e != nil {
		return errors.New("can not find valid .tplus-project.json")
	}

	r, e := ps.client.GetProjectDown(qc.ActiveEndpoint, qc.ActiveToken, qc.ActiveEnvID, pj.ProjectIds[qc.ActiveEnvID])
	if e != nil {
		return e
	}

	Untar(".", r)

	return nil
}

func (ps *ProjectService) Vars() *models.ProjectVars {
	pj, _ := ps.ProjectJson()
	qc := ps.db.GetQuickConf()
	return ps.client.ProjectVars(qc.ActiveEndpoint, qc.ActiveToken, qc.ActiveEnvID, pj.ProjectIds[qc.ActiveEnvID])
}


func (ps *ProjectService) Up() error {
	var buf bytes.Buffer
	compress(".", &buf)
	pj, _ := ps.ProjectJson()
	qc := ps.db.GetQuickConf()
	if _,ok := pj.ProjectIds[qc.ActiveEnvID]; !ok {
		fmt.Println("Project not created in current env")
		fmt.Println("Run: tplus project create")
	}
	return ps.client.ProjectUp(qc.ActiveEndpoint, qc.ActiveToken, qc.ActiveEnvID, pj.ProjectIds[qc.ActiveEnvID], &buf)
}

func (ps *ProjectService) Create(p models.CreateProjectParams) error {
	var buf bytes.Buffer
	compress(".", &buf)
	qc := ps.db.GetQuickConf()
	id := ps.client.ProjectCreate(qc.ActiveEndpoint, qc.ActiveToken, qc.ActiveEnvID, p)
	if id == "" {
		fmt.Println("Error creating Project!")
		os.Exit(1)
	}
	id = strings.Replace(id,"\"","",-1)
	oldJson,pserr := ps.ProjectJson()
	if pserr == nil {
		oldJson.ProjectIds[qc.ActiveEnvID] = id
		ps.SaveProjectJson(oldJson)
	} else {
		newJson := models.ProjectJson{
			ProjectIds: map[string]string{
				qc.ActiveEnvID : id,
			},
			SyncRunning: false,
		}
		ps.SaveProjectJson(&newJson)
	}
	ps.checkGitIgnore() // TODO test
	tj,_ := ps.TplusJson()
	if tj == nil  {
		tpjson := &models.TplusJson{
			ProjectName: p.Name,
			Description: p.Description,
			Tasks:  models.TplusJsonTasks{
				DefaultImage: "registry.gitlab.com/tuliptools/tplus:cli",
			},
		}
		ps.SaveTplusJson(tpjson)
	}
	ps.Up()
	return nil
}


func (ps *ProjectService) UpWihtoutDB(qc *db.QuickConf, pj *models.ProjectJson) error {
	var buf bytes.Buffer
	compress(".", &buf)
	ps.client.ProjectUp(qc.ActiveEndpoint, qc.ActiveToken, qc.ActiveEnvID, pj.ProjectIds[qc.ActiveEnvID], &buf)
	return nil
}

func (ps *ProjectService) SyncStop() error {
	pj, e := ps.ProjectJson()
	if e != nil {
		return e
	}
	pj.SyncRunning = false
	ps.SaveProjectJson(pj)
	return nil
}



func (ps *ProjectService) Sync() error {
	fmt.Print("Starting file sync......")
	qc := ps.db.GetQuickConf()
	ps.db.Close()
	pj, e := ps.ProjectJson()
	if e != nil {
		return e
	}
	pj.SyncRunning = true
	ps.SaveProjectJson(pj)
	for {
		pj, _ := ps.ProjectJson()
		if pj.SyncRunning == false {
			break
		}
		state := ps.calcstate()
		if ps.syncstate != state {
			ps.syncstate = state
			ps.UpWihtoutDB(qc, pj)
		}
		time.Sleep(2 * time.Second)
	}
	return nil
}

func (ps *ProjectService) calcstate() string {
	tmp := ""
	filepath.Walk(".", func(file string, fi os.FileInfo, err error) error {
		if fi.IsDir() {
			tmp = tmp + fi.Name() + fi.Mode().String()
		} else {
			tmp = tmp + fi.Name() + fi.Mode().String() + strconv.Itoa(int(fi.Size())) + fi.ModTime().String()
		}
		return nil
	})

	hash := md5.Sum([]byte(tmp))
	return hex.EncodeToString(hash[:])
}

func Untar(dst string, r io.Reader) error {

	gzr, err := NewReader(r)
	if err != nil {
		return err
	}
	defer gzr.Close()

	tr := tar.NewReader(gzr)

	for {
		header, err := tr.Next()
		switch {
		case err == io.EOF:
			return nil
		case err != nil:
			return err
		case header == nil:
			continue
		}
		target := filepath.Join(dst, header.Name)
		switch header.Typeflag {
		case tar.TypeDir:
			if _, err := os.Stat(target); err != nil {
				if err := os.MkdirAll(target, 0755); err != nil {
					return err
				}
			}
		case tar.TypeReg:
			mod := os.FileMode(0644)
			s, e := os.Stat(target)
			if e == nil {
				mod = s.Mode()
			}
			f, err := os.OpenFile(target, os.O_CREATE|os.O_RDWR, mod)
			if err != nil {
				return err
			}
			if _, err := io.Copy(f, tr); err != nil {
				return err
			}
			f.Close()
		}
	}
}

func compress(src string, buf io.Writer) error {
	zr := NewWriter(buf)
	tw := tar.NewWriter(zr)

	filepath.Walk(src, func(file string, fi os.FileInfo, err error) error {
		if !strings.Contains(file,".tplus-projects") {
			fileClean := file
			header, err := tar.FileInfoHeader(fi, fileClean)
			if err != nil {
				return err
			}

			header.Name = filepath.ToSlash(fileClean)
			if err := tw.WriteHeader(header); err != nil {
				return err
			}
			if !fi.IsDir() {
				data, err := os.Open(file)
				if err != nil {
					return err
				}
				if _, err := io.Copy(tw, data); err != nil {
					return err
				}
			}
		}
		return nil
	})
	if err := tw.Close(); err != nil {
		return err
	}
	if err := zr.Close(); err != nil {
		return err
	}
	return nil
}
