package db

import (
	"github.com/asdine/storm"
	"gitlab.com/tuliptools/TplusCLI/internal/config"
	"gitlab.com/tuliptools/TplusCLI/internal/models"
	"log"
)

type DB struct {
	storm *storm.DB
}

func NewDB(c *config.Config) *DB {
	storm, err := storm.Open(c.DataDir + "/storm.db")
	if err != nil {
		log.Fatal(err)
	}
	d := DB{}
	d.storm = storm
	return &d
}

func (db *DB) Close() {
	db.storm.Close()
}

func (db *DB) AddServer(s *Server) error {
	e := db.storm.Save(s)
	res := db.GetServers()
	if len(res) == 1 {
		db.GetQuickConf()
	}
	return e
}

func (db *DB) GetServers() (res []Server) {
	_ = db.storm.All(&res)
	return res
}

func (db *DB) GetServerByName(name string) (res Server, err error) {
	err = db.storm.One("Name", name, &res)
	return res, err
}

func (db *DB) GetServerById(name string) (res Server, err error) {
	err = db.storm.One("ID", name, &res)
	return res, err
}

func (db *DB) RemoveServer(obj *Server) (err error) {
	err = db.storm.DeleteStruct(obj)
	return err
}

func (db *DB) SetActiveServer(obj *Server) (err error) {
	qc := db.GetQuickConf()
	qc.ActiveServer = obj.ID
	qc.ActiverServerName = obj.Name
	qc.ActiveToken = obj.Token
	qc.ActiveEndpoint = obj.Endpoint
	return db.storm.Save(qc)
}

func (db *DB) SetActiveEnv(obj *models.Environment) (err error) {
	qc := db.GetQuickConf()
	qc.ActiveEnvID = obj.Id
	return db.storm.Save(qc)
}

/*
 Denormalise often used info
*/
func (db *DB) GetQuickConf() *QuickConf {
	res := &QuickConf{}
	db.storm.One("ID", "quickconf", res)
	if res == nil || res.ActiveServer == "" {
		qc := QuickConf{}
		qc.ID = "quickconf"
		servers := db.GetServers()
		if len(servers) >= 1 {
			qc.ActiveServer = servers[0].ID
			qc.ActiverServerName = servers[0].Name
			qc.ActiveToken = servers[0].Token
			qc.ActiveEndpoint = servers[0].Endpoint
		}
		db.storm.Save(&qc)
		return &qc
	}
	return res
}
