package db

import "time"

type QuickConf struct {
	ID                string `storm:"id"`
	ActiveServer      string
	ActiveToken       string
	ActiveEndpoint    string
	ActiverServerName string
	ActiveEnvID       string
}

type Server struct {
	ID        string    `storm:"id"`
	Endpoint  string    `storm:"index"`
	Name      string    `storm:"index"`
	CreatedAt time.Time `storm:"index"`
	LastUsed  time.Time `storm:"index"`
	Username  string
	Token     string
}
