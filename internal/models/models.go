package models

import (
	"time"
)

type RegisterInput struct {
	Username   string `form:"username" json:"username" `
	Password   string `form:"password" json:"password" `
	InviteCode string `form:"invite" json:"invite" binding:"min=0,max=25"`
}

type TokenRequest struct {
	Username string `form:"username" json:"username" `
	Password string `form:"password" json:"password" `
}

type AddEnvInput struct {
	Name        string `form:"name" json:"name" `
	Branch      string `form:"branch" json:"branch" `
	StorageMode string `form:"storageMode" json:"storageMode" `
	Sandboxed   bool   `form:"sandbox" json:"sandbox" `
	RAM         uint   `form:"resourcesMemory" json:"resourcesMemory" `
	CPU         uint   `form:"resourcesCPU" json:"resourcesCPU" `
	Shared      bool   `form:"shared" json:"shared" `
}

type Token struct {
	Token string
}

type ContainerLS struct {
	Name    string
	ID      string
	Image   string
	Command string
	Created string
	Status  string
}

type Info struct {
	LoggedIn   bool
	ServerMode string
	UserID     string
	UserRole   string
}

type Environment struct {
	Id              string
	Created         time.Time
	Name            string
	Tezos           TezosNode
	CreatedByUserId string
	Shared          bool
	Consoles        map[string]Container
}

type TezosNode struct {
	Sandboxed     bool
	Branch        string
	Status        string
	HeadLevel     uint64
	Container     Container
	DataDir       string
	ClientDataDir string
	SharedDir     string
	SnapshotDir   string
	HistoryMode   string
	Endpoint      string
}

type Container struct {
	Id              string
	Name            string
	Ips             []string
	Network         string
	NetworkID       string
	UpdateAvailable bool
	Status          string
	Resources       Resources
}

type Metric struct {
	Time          time.Time
	EnvironmentId string
	Source        string
	Metric        string
	Value         float64
}

type MetricSimple struct {
	Time  time.Time
	Value float64
}

type Resources struct {
	CPUCount  uint // in milliCPU
	MemoryMax uint // in byte
}

type ProjectJson struct {
	ProjectIds map[string]string
	SyncRunning bool
}

type TplusJson struct {
	ProjectName string `json:"ProjectName"`
	Description string `json:"Description"`
	Tasks TplusJsonTasks        `json:"Tasks"`
}

type TplusJsonTasks struct {
	DefaultImage string `json:"DefaultImage"`
}


type Project struct {
	Created     time.Time         `json:"Created"`
	LastRead    time.Time         `json:"LastRead"`
	LastSave    time.Time         `json:"LastSave"`
	ID          string            `json:"Id"`
	EnvID       string            `json:"EnvId"`
	Name        string            `json:"Name"`
	Descritpion string            `json:"Descritpion"`
	Alias       string            `json:"Alias"`
	SetupDone   bool              `json:"SetupDone"`
	Source      string            `json:"Source"`
	Template    string            `json:"Template"`
	Variables   map[string]string `json:"Variables"`
}

type ProjectTask struct {
	LastRead  time.Time     `json:"LastRead"`
	LastSave  time.Time     `json:"LastSave"`
	ProjectID string        `json:"ProjectId"`
	Name      string        `json:"Name"`
	ID        string        `json:"Id"`
	Created   time.Time     `json:"Created"`
	Logs      []interface{} `json:"Logs"`
	Result    bool          `json:"Result"`
	Finished  bool          `json:"Finished"`
	Cmd       []interface{} `json:"Cmd"`
	LastSaved time.Time     `json:"LastSaved"`
	Tasks     []struct {
		Include    string      `json:"Include"`
		Name       string      `json:"Name"`
		Image      string      `json:"Image"`
		Command    interface{} `json:"Command"`
		Bash       []string    `json:"Bash"`
		Timeout    string      `json:"Timeout"`
		Log        interface{} `json:"Log"`
		WaitBlocks int         `json:"WaitBlocks"`
		Blocking   string      `json:"Blocking"`
		Verbose    string      `json:"Verbose"`
		Register   string      `json:"Register"`
		IsSet      string      `json:"IsSet"`
		IsNotSet   string      `json:"IsNotSet"`
		HideLog    string      `json:"HideLog"`
		DebugVars  string      `json:"DebugVars"`
		ParseJSON  interface{} `json:"ParseJson"`
	} `json:"Tasks"`
	Filename      string      `json:"Filename"`
	Error         string      `json:"Error"`
	CustomDockers interface{} `json:"CustomDockers"`
	Image         string      `json:"Image"`
	TplusInfo     struct {
		ProjectName string `json:"ProjectName"`
		Description string `json:"Description"`
		Tasks       struct {
			DefaultImage string `json:"DefaultImage"`
		} `json:"Tasks"`
	} `json:"TplusInfo"`
	Variables struct {
	} `json:"Variables"`
	InitVariables interface{} `json:"InitVariables"`
}

type CreateProjectParams struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Alias       string `json:"alias"`
	Source      string `json:"source"`
	Project     string `json:"project"`
}


type ProjectVars struct {
	Variables []struct {
		Name        string `json:"Name"`
		Description string `json:"Description"`
		Type        string `json:"Type"`
		Value       string `json:"Value"`
		Editable    bool   `json:"Editable"`
	} `json:"Variables"`
	Compiled map[string]string `json:"Compiled"`
}