package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
)

func getSync(c *dig.Container) *cobra.Command {
	var serverRoot = &cobra.Command{
		Use:   "sync",
		Short: "Tplus-Server file sync",
	}
	serverRoot.AddCommand(sync(c))
	serverRoot.AddCommand(stop(c))
	return serverRoot
}

func sync(c *dig.Container) *cobra.Command {
	var bash = &cobra.Command{
		Use:   "start",
		Short: "Start sync process",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(p *service.ProjectService) {
				p.Sync()
			})
		},
	}
	return bash
}

func stop(c *dig.Container) *cobra.Command {
	var bash = &cobra.Command{
		Use:   "stop",
		Short: "Stop sync process",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(p *service.ProjectService) {
				p.SyncStop()
			})
		},
	}
	return bash
}
