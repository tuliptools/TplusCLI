package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
)

func getClient(c *dig.Container) *cobra.Command {
	var clientRoot = &cobra.Command{
		Use:   "client",
		Short: "Manage client of current Environment",
	}

	clientRoot.AddCommand(getClientBash(c))

	return clientRoot
}

func getClientBash(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "bash",
		Short: "Open a Console inside the client container",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(env *service.EnvService) {
				env.GetConsole("client")
			})
		},
	}
	return envLS
}
