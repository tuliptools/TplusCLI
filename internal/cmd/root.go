package cmd

import (
	"github.com/spf13/cobra"
	"go.uber.org/dig"
)

func GetRootCmd(c *dig.Container) *cobra.Command {

	var rootCmd = &cobra.Command{
		Use:   "tp",
		Short: "tp is the Tplus CLI Tool",
	}

	rootCmd.AddCommand(getServer(c))
	rootCmd.AddCommand(getEnv(c))
	rootCmd.AddCommand(getClient(c))
	rootCmd.AddCommand(get(c))
	rootCmd.AddCommand(getNode(c))
	rootCmd.AddCommand(getSync(c))
	rootCmd.AddCommand(getProject(c))
	rootCmd.AddCommand(getVagrant(c))
	setTasks(c, rootCmd)
	return rootCmd

}
