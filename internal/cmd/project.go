package cmd

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/TplusCLI/internal/db"
	"gitlab.com/tuliptools/TplusCLI/internal/models"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
	"os"
	"strings"
	"time"
)

func getProject(c *dig.Container) *cobra.Command {
	var projectRoot = &cobra.Command{
		Use:   "project",
		Short: "Manage Projects",
	}

	projectRoot.AddCommand(initProject(c))
	projectRoot.AddCommand(upProject(c))
	projectRoot.AddCommand(downProject(c))
	projectRoot.AddCommand(createProject(c))
	projectRoot.AddCommand(vars(c))
	projectRoot.AddCommand(secrets(c))

	return projectRoot
}

func initProject(c *dig.Container) *cobra.Command {
	var bash = &cobra.Command{
		Use:   "init",
		Short: "initialize Project",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("usage: tplus project init [projectID]")
				return
			}
			_ = c.Invoke(func(env *service.EnvService, projectS *service.ProjectService, db *db.DB) {
				active := env.GetActiveEnv()
				qc := db.GetQuickConf()
				if active.Id == "" {
					fmt.Println("could not get active Environment...")
					os.Exit(1)
				}

				p, e := projectS.Init(args[0])
				if e != nil || p.ID != args[0] {
					fmt.Println("error getting Project")
					return
				}

				pj, e := projectS.ProjectJson()
				val,_ := pj.ProjectIds[qc.ActiveEnvID]
				if e == nil || val != "" {
					fmt.Println("Seems like there is already a project initialized...")
					return
				}

				pj.ProjectIds[qc.ActiveEnvID] = p.ID

				e = projectS.SaveProjectJson(pj)
				if e != nil {
					fmt.Println(e)
					return
				}

				e = projectS.Down()
				if e != nil {
					fmt.Println(e)
					return
				}
				time.Sleep(1 * time.Second)
				fmt.Println("Ready!")

			})
		},
	}

	return bash
}

func upProject(c *dig.Container) *cobra.Command {
	var bash = &cobra.Command{
		Use:   "up",
		Short: "Send local data to Tplus-Server",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(p *service.ProjectService) {
				e := p.Up()
				if e != nil {
					fmt.Println(e)
					return
				}
			})
		},
	}
	return bash
}

func downProject(c *dig.Container) *cobra.Command {
	var bash = &cobra.Command{
		Use:   "down",
		Short: "Download Remote Data",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(p *service.ProjectService) {
				e := p.Down()
				if e != nil {
					fmt.Println(e)
					return
				}
			})
		},
	}

	return bash
}


func createProject(c *dig.Container) *cobra.Command {
	var create = &cobra.Command{
		Use:   "create",
		Short: "Create an new project from local files",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(p *service.ProjectService) {

				tpjson,_ := p.TplusJson()

				params := models.CreateProjectParams{
					Name: "",
					Description: "",
				}

				if tpjson != nil {
					params.Name = tpjson.ProjectName
					params.Description = tpjson.Description
				}

				prompt := promptui.Prompt{
					Label: "Project Name",
					Default: params.Name,
				}
				name, e := prompt.Run()
				if e == nil {
					params.Name = name
				} else {
					fmt.Println(e)
					os.Exit(1)
				}

				prompt2 := promptui.Prompt{
					Label: "Project Description",
					Default: params.Description,
				}
				desc, e := prompt2.Run()
				if e == nil {
					params.Description = desc
				} else {
					fmt.Println(e)
					os.Exit(1)
				}

				e = p.Create(params)
				if e != nil {
					fmt.Println(e)
					return
				}
			})
		},
	}

	return create
}


func vars(c *dig.Container) *cobra.Command {
	export := false
	var bash = &cobra.Command{
		Use:   "vars",
		Short: "Get Project variables",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(p *service.ProjectService) {
				v := p.Vars()
				if v == nil {
					fmt.Println("no variables available")
				} else {
					for k,v := range v.Compiled {
						if !strings.Contains(strings.ToLower(k),"secret") {
							if export {
								fmt.Print("export ")
								k = strings.ToUpper(k)
								k = strings.Replace(k,".","_",-1)
							}
							fmt.Println(k + "=" + v)
						}
					}
				}
			})
		},
	}
	bash.PersistentFlags().BoolVarP(&export,"export","e",false,"Show as export command")

	return bash
}

func secrets(c *dig.Container) *cobra.Command {
	export := false
	var bash = &cobra.Command{
		Use:   "secrets",
		Short: "Get Project variables-secrets",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(p *service.ProjectService) {
				v := p.Vars()
				if v == nil {
					fmt.Println("no variables available")
				} else {
					for k,v := range v.Compiled {
						if strings.Contains(strings.ToLower(k),"secret") {
							if export {
								fmt.Print("export ")
								k = strings.ToUpper(k)
								k = strings.Replace(k,".","_",-1)
							}
							fmt.Println(k + "=" + v)
						}
					}
				}
			})
		},
	}
	bash.PersistentFlags().BoolVarP(&export,"export","e",false,"Show as export command")
	return bash
}