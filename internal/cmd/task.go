package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
	"io/ioutil"
	"strings"
)

func setTasks(c *dig.Container, cmd *cobra.Command) {

	files, err := ioutil.ReadDir("./tasks/")
	if err != nil {
		return
	}

	for _, f := range files {
		name := strings.Replace(f.Name(), ".yml", "", 1)
		cmd.AddCommand(getTask(c, f.Name(), name))
	}

}

func getTask(c *dig.Container, file, name string) *cobra.Command {
	var task = &cobra.Command{
		Use:   name,
		Short: "Run TaskCollection " + file,
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(task *service.TaskService) {
				task.Run(file)
			})
		},
	}
	return task
}
