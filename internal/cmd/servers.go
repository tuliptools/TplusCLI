package cmd

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/olekukonko/tablewriter"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/TplusCLI/internal/db"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
	"os"
	"regexp"
)

func getServer(c *dig.Container) *cobra.Command {
	var serverRoot = &cobra.Command{
		Use:   "server",
		Short: "Configure Tplus Servers",
	}
	serverRoot.AddCommand(getServerLS(c))
	serverRoot.AddCommand(getServerAdd(c))
	serverRoot.AddCommand(getServerRm(c))
	serverRoot.AddCommand(getServerUse(c))
	return serverRoot
}

func getServerLS(c *dig.Container) *cobra.Command {
	var serverLS = &cobra.Command{
		Use:   "ls",
		Short: "List all Servers",
		Run: func(cmd *cobra.Command, args []string) {
			err := c.Invoke(func(db *db.DB, log *logrus.Logger) {
				servers := db.GetServers()

				if len(servers) == 0 {
					fmt.Println("No Servers configured,")
					fmt.Println("maybe try tp server add?")
					return
				}

				var data [][]string

				for _, i := range servers {
					data = append(data, []string{
						i.Name,
						i.Endpoint,
						i.Username,
						i.CreatedAt.Format("Mon Jan 02 15:04:05"),
						i.LastUsed.Format("Mon Jan 02 15:04:05"),
					})
				}

				table := tablewriter.NewWriter(os.Stdout)
				table.SetTablePadding("  ")
				table.SetBorder(false)
				table.SetHeader([]string{"Name", "Endpoint", "User", "Created", "Last used"})
				for _, v := range data {
					table.Append(v)
				}
				fmt.Println("")
				table.Render()
				fmt.Println("")

			})
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		},
	}
	return serverLS
}

func getServerUse(c *dig.Container) *cobra.Command {
	var serverUse = &cobra.Command{
		Use:   "use",
		Short: "Set active Server",
		Run: func(cmd *cobra.Command, args []string) {
			err := c.Invoke(func(db *db.DB, log *logrus.Logger) {
				servers := db.GetServers()

				if len(args) == 1 {
					for _, i := range servers {
						if i.Name == args[0] {
							e := db.SetActiveServer(&i)
							if e != nil {
								fmt.Println("Error setting active Server")
								return
							}
							fmt.Println("Success")
							return
						}
					}
					fmt.Println("Could not find Server!")
					return
				}

				var names []string

				for _, i := range servers {
					names = append(names, i.Name)
				}

				prompt := promptui.Select{
					Label: "Select Server",
					Items: names,
				}
				_, name, _ := prompt.Run()

				for _, i := range servers {
					if i.Name == name {
						e := db.SetActiveServer(&i)
						if e != nil {
							fmt.Println("Error setting active Server")
							return
						}
						fmt.Println("Success")
						return
					}
				}
				fmt.Println("Could not find Server!")

			})
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		},
	}
	return serverUse
}

func getServerAdd(c *dig.Container) *cobra.Command {
	apiUrl := ""
	password := ""
	username := ""
	name := ""
	var err error

	var serverAdd = &cobra.Command{
		Use:   "add",
		Short: "Add a new Server",
		Run: func(cmd *cobra.Command, args []string) {
			err := c.Invoke(func(db *db.DB, log *logrus.Logger, server *service.ServerService) {

				validateUrl := func(input string) error {
					valid := regexp.MustCompile(`http?://(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)`)
					if !valid.MatchString(input) {
						return errors.New("must be a valid url format, like http://example.com:8085/")
					}
					return nil
				}

				validateString := func(input string) error {
					if len(input) <= 4 {
						errors.New("Must be at least 4 characters")
					}
					return nil
				}

				if len(apiUrl) == 0 {
					apiUrlP := promptui.Prompt{
						Label:    "Api Endpoint: ",
						Validate: validateUrl,
					}
					apiUrl, err = apiUrlP.Run()
				}

				if len(name) == 0 {
					nameP := promptui.Prompt{
						Label:    "Name: ",
						Validate: validateString,
					}
					name, err = nameP.Run()
				}

				// check if is a no-user-auth server
				err = server.AddServer(apiUrl, "dummyUser", "dummyPass", name)
				if err == nil {
					fmt.Println("Server was added!")
					return
				}

				if len(username) == 0 {
					usernameP := promptui.Prompt{
						Label:    "Username: ",
						Validate: validateString,
					}
					username, err = usernameP.Run()
				}

				if len(password) == 0 {
					passwordP := promptui.Prompt{
						Label:    "Password: ",
						Validate: validateString,
						Mask:     '*',
					}
					password, err = passwordP.Run()
				}

				if err != nil {
					fmt.Printf("Prompt failed %v\n", err)
					return
				}

				fmt.Println(name)
				err = server.AddServer(apiUrl, username, password, name)

				if err != nil {
					fmt.Println("Could not add Server")
					fmt.Println(err)
					return
				}

			})
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		},
	}

	serverAdd.Flags().StringVarP(&apiUrl, "apiUrl", "e", "", "Endpoint of the Server")
	serverAdd.Flags().StringVarP(&password, "password", "p", "", "User password")
	serverAdd.Flags().StringVarP(&username, "username", "u", "", "User username")
	serverAdd.Flags().StringVarP(&name, "name", "n", "", "Name of the server")

	return serverAdd
}

func getServerRm(c *dig.Container) *cobra.Command {
	name := ""
	var err error

	var serverRM = &cobra.Command{
		Use:   "rm",
		Short: "Remove an existing Server form the local config",
		Run: func(cmd *cobra.Command, args []string) {
			err := c.Invoke(func(db *db.DB, log *logrus.Logger, server *service.ServerService) {

				servers := db.GetServers()
				if len(servers) == 0 {
					fmt.Println("No servers configured")
					return
				}

				if len(name) == 0 {
					var items []string
					for _, i := range servers {
						items = append(items, i.Name)
					}

					prompt := promptui.Select{
						Label: "Select Server",
						Items: items,
					}
					_, name, err = prompt.Run()

					if err != nil {
						fmt.Printf("Prompt failed %v\n", err)
						return
					}
				} else {
					for l, i := range servers {
						if i.Name == name {
							break
						}
						if l == len(servers) {
							fmt.Println("server not found")
							return
						}
					}
				}

				s := server.GetServerByName(name)
				e := server.RemoveServer(s)

				if e == nil {
					fmt.Println("Success")
				} else {
					fmt.Println(e)
				}

			})
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		},
	}

	serverRM.Flags().StringVarP(&name, "name", "n", "", "Name of the server")

	return serverRM
}
