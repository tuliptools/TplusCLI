package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
	"os"
)

func getNode(c *dig.Container) *cobra.Command {
	var nodeRoot = &cobra.Command{
		Use:   "node",
		Short: "Manage node of current Environment",
	}

	nodeRoot.AddCommand(getNodeLogs(c))
	nodeRoot.AddCommand(getNodeBash(c))

	return nodeRoot
}

func getNodeLogs(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "logs",
		Short: "Get Tezos node logs",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(env *service.EnvService) {
				active := env.GetActiveEnv()
				logs, err := env.GetLogs(active, 100)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				for _, l := range logs {
					fmt.Println(l)
				}
			})
		},
	}
	return envLS
}

func getNodeBash(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "bash",
		Short: "Open a Console inside the node container",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(env *service.EnvService) {
				env.GetConsole("node")
			})
		},
	}
	return envLS
}
