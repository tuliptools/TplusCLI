package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
)

func get(c *dig.Container) *cobra.Command {
	var envRoot = &cobra.Command{
		Use:   "get",
		Short: "get - provides little helper functions",
	}

	envRoot.AddCommand(getHead(c))

	return envRoot
}

func getHead(c *dig.Container) *cobra.Command {
	var head = &cobra.Command{
		Use:   "head",
		Short: "Get head of current ",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(ev *service.EnvService) {
				fmt.Println("TODO")
			})
		},
	}
	return head
}
