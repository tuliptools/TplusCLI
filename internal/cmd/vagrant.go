package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
)

func getVagrant(c *dig.Container) *cobra.Command {
	var clientRoot = &cobra.Command{
		Use:   "vm",
		Short: "Manage Tplus in a box",
	}

	clientRoot.AddCommand(getVmUp(c))
	clientRoot.AddCommand(getVmdown(c))
	clientRoot.AddCommand(getVmDestroy(c))

	return clientRoot
}

func getVmUp(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "up",
		Short: "Start Tplus Developer VM",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(vm *service.VmService) {
				vm.Up()
			})
		},
	}
	return envLS
}

func getVmdown(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "down",
		Short: "Stop Tplus Developer VM",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(vm *service.VmService) {
				vm.Down()
			})
		},
	}
	return envLS
}

func getVmDestroy(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "destroy",
		Short: "delete VM",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(vm *service.VmService) {
				vm.Destroy()
			})
		},
	}
	return envLS
}
