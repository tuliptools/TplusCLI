package cmd

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/TplusCLI/internal/db"
	"gitlab.com/tuliptools/TplusCLI/internal/models"
	"gitlab.com/tuliptools/TplusCLI/internal/service"
	"go.uber.org/dig"
	"os"
)

func getEnv(c *dig.Container) *cobra.Command {
	var envRoot = &cobra.Command{
		Use:   "env",
		Short: "Manage Environments",
	}

	envRoot.AddCommand(getEnvLs(c))
	envRoot.AddCommand(getEnvUse(c))
	envRoot.AddCommand(getEnvInspect())
	envRoot.AddCommand(getEnvStart(c))
	envRoot.AddCommand(getEnvStop(c))
	envRoot.AddCommand(getEnvRestart(c))
	envRoot.AddCommand(getEnvClean(c))
	envRoot.AddCommand(getEnvDestroy(c))

	return envRoot
}

func getEnvLs(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "ls",
		Short: "list available environments",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(ev *service.EnvService) {
				envs, err := ev.GetEnvironments()

				if err != nil {
					fmt.Println("could not get envs!")
					os.Exit(1)
				}
				var data [][]string

				for _, i := range envs {
					data = append(data, []string{
						i.Id,
						i.Name,
						i.Tezos.Branch,
						i.Tezos.Status,
						i.Tezos.HistoryMode,
					})
				}

				table := tablewriter.NewWriter(os.Stdout)
				table.SetTablePadding("  ")
				table.SetBorder(false)
				table.SetHeader([]string{"ID", "Name", "Network", "Node Status", "HistoryMode"})
				for _, v := range data {
					table.Append(v)
				}
				fmt.Println("")
				table.Render()
				fmt.Println("")
			})
		},
	}
	return envLS
}

func getEnvUse(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "use",
		Short: "set default env",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(db *db.DB, ps *service.ProjectService) {
				env := selectEnv(c, args)
				if db.SetActiveEnv(env) == nil {
					// check if we are in a project
					// if yes, create current project in new env
					tj,e := ps.TplusJson()
					if e == nil && tj.ProjectName != ""{
						pj,e2 := ps.ProjectJson()
						if e2 == nil {
							if _,ok := pj.ProjectIds[env.Id]; !ok {
								e := ps.Create(models.CreateProjectParams{
									Name: tj.ProjectName,
									Description: tj.Description,
								})
								if e == nil {
									fmt.Println("Created current Project in environment", env.Name)
								}
							}
						}
					}
					fmt.Println("switched to", env.Name)

				} else {
					fmt.Println("Error")
				}

			})
		},
	}
	return envLS
}

func selectEnv(c *dig.Container, args []string) *models.Environment {
	selected := models.Environment{}

	if len(args) == 1 {
		c.Invoke(func(ev *service.EnvService) {
			envs, err := ev.GetEnvironments()
			if err != nil {
				fmt.Println("Could not get Environments")
				os.Exit(1)
			}
			for n, i := range envs {
				if i.Name == args[0] {
					selected = i
					break
				}
				if n == len(envs) {
					fmt.Println("Cound not find Environment ", args[0])
					os.Exit(1)
				}
			}
		})
	} else {
		_ = c.Invoke(func(env *service.EnvService) {
			envs, err := env.GetEnvironments()

			if err != nil {
				fmt.Println("Could not load environments")
				os.Exit(1)
			}

			var envNames []string
			for _, i := range envs {
				envNames = append(envNames, i.Name)
			}
			prompt := promptui.Select{
				Label: "Environment",
				Items: envNames,
			}
			_, selectedName, err := prompt.Run()

			for _, i := range envs {
				if i.Name == selectedName {
					selected = i
					break
				}
			}

		})
	}

	return &selected
}

func getEnvStop(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "stop",
		Short: "Stop Env and Tezos node",
		Run: func(cmd *cobra.Command, args []string) {
			selected := selectEnv(c, args)
			fmt.Println("Stopping environment ", selected.Name, "(", selected.Id, ") ...")

			_ = c.Invoke(func(env *service.EnvService) {
				e := env.StopEnvironment(selected)
				if e != nil {
					fmt.Println("Error")
				}
			})
		},
	}
	return envLS
}

func getEnvStart(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "start",
		Short: "Start Env and Tezos node",
		Run: func(cmd *cobra.Command, args []string) {
			selected := selectEnv(c, args)
			fmt.Println("Starting environment ", selected.Name, "(", selected.Id, ") ...")

			_ = c.Invoke(func(env *service.EnvService) {
				e := env.StartEnvironment(selected)
				if e != nil {
					fmt.Println("Error")
				}
			})
		},
	}
	return envLS
}

func getEnvRestart(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "restart",
		Short: "restart Env and Tezos node",
		Run: func(cmd *cobra.Command, args []string) {
			selected := selectEnv(c, args)
			fmt.Println("Restarting environment ", selected.Name, "(", selected.Id, ") ...")

			_ = c.Invoke(func(env *service.EnvService) {
				e := env.RestartEnvironment(selected)
				if e != nil {
					fmt.Println("Error")
				}
			})
		},
	}
	return envLS
}

func getEnvClean(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "clean",
		Short: "Clean datadir",
		Run: func(cmd *cobra.Command, args []string) {
			selected := selectEnv(c, args)
			fmt.Println("Cleaning environment ", selected.Name, "(", selected.Id, ") ...")

			_ = c.Invoke(func(env *service.EnvService) {
				e := env.CleanEnvironment(selected)
				if e != nil {
					fmt.Println("Error")
				}
			})
		},
	}
	return envLS
}

func getEnvDestroy(c *dig.Container) *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "destroy",
		Short: "Destroy Environment and all related resources",
		Run: func(cmd *cobra.Command, args []string) {
			selected := selectEnv(c, args)
			fmt.Println("Destroying environment ", selected.Name, "(", selected.Id, ") ...")

			_ = c.Invoke(func(env *service.EnvService) {
				e := env.DestroyEnvironment(selected)
				if e != nil {
					fmt.Println("Error")
				}
			})
		},
	}
	return envLS
}

func getEnvInspect() *cobra.Command {
	var envLS = &cobra.Command{
		Use:   "inspect",
		Short: "Show env Details",
		Run: func(cmd *cobra.Command, args []string) {
			// make watchable
			fmt.Println("TODO")
			// Tezos status branch blockheight container
			// rpc endpoint
			// uptime
			// # peers if up
			// installed plugins
			// last 5 tasks
			// cpu load
			// ram usage
			// last block
		},
	}
	return envLS
}
