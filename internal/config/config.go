package config

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"io/ioutil"
	"os"
)

func NewConfig(l *logrus.Logger) *Config {
	c,e := getConfigFileViper(l)
	if e != nil {
		c2,e := createConfigFileDefaults(l)
		if e != nil {
			panic(e)
		}
		return c2
	}
	return c
}



func getConfigFileViper(l *logrus.Logger) (*Config,error) {
	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/tplus/cli/")
	viper.AddConfigPath("$HOME/.tplus/cli/")
	viper.AddConfigPath(".")
	l.Trace("Read config file")
	err := viper.ReadInConfig()
	if err != nil {
		return nil,err
	}
	c := Config{}
	e := viper.Unmarshal(&c)
	if e != nil {
		l.Error("Error reading config file: ", e.Error())
	}
	return &c,e
}

func createConfigFileDefaults(l *logrus.Logger) (*Config,error){
	l.Trace("Start create config file from Defaults")
	c := Config{}
	homedir,e := os.UserHomeDir()
	if e != nil {
		l.Error("Error Getting user HomeDir: ", e.Error())
		return &c,e
	}
	c.DataDir = homedir + "/.tplus/cli/"

	e = os.MkdirAll(c.DataDir,0755)
	if e != nil {
		l.Error("Error creating path for config file: ", e.Error())
		return nil,e
	}

	e = saveConfigFile(&c, homedir + "/.tplus/cli/config.json")

	if e != nil {
		l.Error("Error writing new Config File: ",e.Error())
		return nil,e
	}

	return &c,nil
}


func saveConfigFile(c *Config, file string) error{
	bytes,_ := json.Marshal(c)
	return ioutil.WriteFile(file,bytes,0755)
}


type Config struct {
	DataDir string
}