module gitlab.com/tuliptools/TplusCLI

go 1.13

require (
	github.com/DataDog/zstd v1.4.5 // indirect
	github.com/Sereal/Sereal v0.0.0-20200708070659-bf04e8628e08 // indirect
	github.com/asdine/storm v2.1.2+incompatible
	github.com/bmatcuk/go-vagrant v1.4.2
	github.com/containerd/containerd v1.3.6
	github.com/golang/snappy v0.0.1 // indirect
	github.com/manifoldco/promptui v0.7.0
	github.com/nsf/termbox-go v0.0.0-20190817171036-93860e161317
	github.com/olekukonko/tablewriter v0.0.4
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.5.0
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	go.etcd.io/bbolt v1.3.3 // indirect
	go.uber.org/dig v1.7.0
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092
	golang.org/x/sys v0.0.0-20191112214154-59a1497f0cea // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/yaml.v2 v2.2.5 // indirect
)
