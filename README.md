# Tplus CLI Client


This is the CLI to for tplus,in order to use this you also need an instance of `tplus-server`
running on your local machine, or a remote one, see http://tplus.dev for more instructions.

## Installation 


```
$ wget https://files.tplus.dev/tplus
$ chmod +x tplus
$ sudo mv tplus /usr/local/bin/tplus
```

## Connect to `tplus-server`

Tplus is set up so that the CLI tool and the server instance can be run on different machines,
If you use tplus the first time you need to tell it which server to connect to, for example:


```
$ tplus server add
Api Endpoint: : http://locahost:8558█
Username: : user█
Password: : ********█
Name: : local█
local
```

## Environemnts

To list all available environments run:

```
$ tplus env ls

           ID          |  NAME     | NETWORK | NODE STATUS | HISTORYMODE  
-----------------------+-----------+---------+-------------+--------------
  env-Sft9n4Vbz7Mn5vee | MySandbox | sandbox | running     | archive      
```

Many commands, for managing projects for example, depend on having a default environment set.
You can set your active environment with

```
$ tplus env use
```

## Projects

To initialize a Project run

```
$ tplus project init <project-id>
```

Note that this will also start a filesync-process in the background,
you can manage this sync process with

```
$ tplus sync ...
```

and force manual up/downloads with: 

```
$ tplus project up|down
```

### Tasks

You can run tasks defined in your project ( under `./tasks/`) using:

```
$ tplus <taskname>
```

This will execute your Task on the `tplus-server`, but show logs in your Terminal and also update any files that changed


### Documentation:

Current documentation for Tplus is available at [tplus.dev](https://tplus.dev)

### Join our Community

For feedback and questions, please find us @

* [Telegram](https://t.me/tuliptools)
* [Twitter](https://twitter.com/TulipToolsOU)
* [tezos-dev Slack](https://tezos-dev.slack.com/#/)

### Related Repositories:
* [Demo Projects](https://gitlab.com/tuliptools/tplusdemoprojects)
* [Tplus Main Repo](https://gitlab.com/tuliptools/tplus)
* [User Interface](https://gitlab.com/tuliptools/tplusgui)
* [CLI Tool](https://gitlab.com/tuliptools/TplusCLI)
* [Plugins](https://gitlab.com/tuliptools/TplusPlugins)

Tplus is developed by [TulipTools](https://tulip.tools/)

![Tulip Logo](https://tulip.tools/wp-content/uploads/2020/06/tulip_small-2.png)
